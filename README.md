# MeFit - Training App
![build](https://img.shields.io/badge/build-passing-green)

MeFit is an angular app for setting a 7 days long training goal.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.1.3.

The project is part of a complete solution. To see the backend part of the project, click [here](https://gitlab.com/hhoan/mefit-be).


## Description
MeFit is a part of the Noroff Accelerate course and is the final assignment that will mark our completion of the .NET fullstack program.

This project is a fitness tracker app where users can view registered programs and add it to their weekly workout. The Goal Dashboard fetches the goal of the user and displays the program with the different workouts and their exercises. The users can track the progress in the dashboard by checking off the workouts when it is complete.

## Installation
You can download the repository with: 
```
git clone git@gitlab.com:Vanessatpm/mefit-fe.git 
```

## Usage
Before proceeding make sure the [backend](https://gitlab.com/hhoan/mefit-be) is up and running. Currently the code points the backend project that is hosted online, if you wish to run the entire solution locally you will have to change all of the API URLs to point to https://localhost:xxxx/ rather than https://mefit-webapp.azurewebsites.net/, which it currently points to.

Download packages and dependencies by running:
```
npm install
```
Run the application by running:
```
ng serve
```
Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


## Dependencies
[![git](https://img.shields.io/badge/Git-F05032.svg?style=for-the-badge&logo=Git&logoColor=white)](https://git-scm.com/)
[![angular](https://img.shields.io/badge/Angular-DD0031.svg?style=for-the-badge&logo=Angular&logoColor=white)](https://angular.io/)
[![npm](https://img.shields.io/badge/npm-CB3837.svg?style=for-the-badge&logo=npm&logoColor=white)](https://www.npmjs.com/)
[![NODE.js](https://img.shields.io/badge/Node.js-339933.svg?style=for-the-badge&logo=nodedotjs&logoColor=white)](https://nodejs.org/en)
[![VS Code](https://img.shields.io/badge/Visual%20Studio%20Code-007ACC.svg?style=for-the-badge&logo=Visual-Studio-Code&logoColor=white)](https://code.visualstudio.com/)

## Contributing

This project is unfortunately not open for contribution.

## Contributors
- [Abdullah Hussain](https://gitlab.com/abdullah-NO)
- [Ha Hoang](https://gitlab.com/hhoan) 
- [Vanessa Pastén-Millán](https://gitlab.com/Vanessatpm)
- [Lasse Steinnes](https://gitlab.com/lasse-steinnes)
- Ola Ellevold (UX/UI)




