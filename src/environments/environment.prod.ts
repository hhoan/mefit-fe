export const environment = {
  production: true,
  apiProgram: 'https://mefit-webapp.azurewebsites.net/api/v1/fitness-programs',
  apiExercises: 'https://mefit-webapp.azurewebsites.net/api/v1/workouts', // /3/exercises
  apiUrlUserProfile:
    'https://mefit-webapp.azurewebsites.net/api/v1/user-profiles',
  apiGoals: 'https://mefit-webapp.azurewebsites.net/api/v1/goals',
  apiWorkouts: 'https://mefit-webapp.azurewebsites.net/api/v1/workouts',
};

// OLD :  apiExercises: 'https://mefit-webapp.azurewebsites.net/api/v1/exercises',
