export const environment = {
  production: false,
  apiPrograms: 'https://mefit-webapp.azurewebsites.net/api/v1/fitness-programs',
  //apiExercises: 'https://mefit-webapp.azurewebsites.net/api/v1/exercises',
  apiUserProfile: 'https://mefit-webapp.azurewebsites.net/api/v1/user-profiles',
  apiExercises: 'https://mefit-webapp.azurewebsites.net/api/v1/workouts', // /3/exercises,
  apiUrlUserProfile:
    'https://mefit-webapp.azurewebsites.net/api/v1/user-profiles',
  apiGoals: 'https://mefit-webapp.azurewebsites.net/api/v1/goals',
  apiWorkouts: 'https://mefit-webapp.azurewebsites.net/api/v1/workouts',
};
