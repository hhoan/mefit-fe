import { ProgramSummary } from './program.model';
import { Workout } from './workout.model';
// import { Workout } from './workout.model';

export interface Goal {
  goal_id: number;
  endDate: string;
  achieved: boolean;
  program: Program;
  /* 
  //* 
program: ProgramSummary;
workouts: Workout[]; */
}

export interface Program {
  program_id: number;
  name: string;
  category_id: number;
  workouts: Workout[];
}
