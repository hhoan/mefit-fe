export interface ExerciseModel {
  exercise_id: number;
  name: string;
  description: string;
  muscleGroup_Id: number;
  image?: string;
  sets: number[];
}
