export interface UserProfile {
    id: string;
    weight: number;
    height: number;
    fitnessLevel: string;
    fitnessGoal: string;
    //Goals?: UserGoals[];
}

export interface UserGoals {
    goal_id: number;
    endDate: Date;
    achieved: boolean;
}