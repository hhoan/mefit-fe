export interface Workout {
  workout_id: number;
  workoutName: string;
  complete: boolean;
  workoutCategory_id: number;
}
