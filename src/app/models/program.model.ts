import { ProgramCategory } from './program-category.model';
import { Workout } from './workout.model';

export interface ProgramSummary {
  program_id: number;
  name: string;
  category_id: number;
}

export interface Program extends ProgramSummary {
  programCategory: ProgramCategory;
  workouts: Workout[];
}
