import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage implements OnInit {
  constructor(private router: Router) {}

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  ngOnInit(): void {
    if (this.authenticated) {
      // TODO: legge til if-else-blokk som sjekker om bruker er registrert i databasen vår
      // if ***user exists***
      //this.router.navigateByUrl('/goal');
      // else
      this.router.navigateByUrl('/registration');
    }
  }

  doLogin(): void {
    keycloak.login();
  }
}
