import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessGoalPage } from './fitness-goal.page';

describe('FitnessGoalPage', () => {
  let component: FitnessGoalPage;
  let fixture: ComponentFixture<FitnessGoalPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitnessGoalPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FitnessGoalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
