import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeightPage } from './height.page';

describe('HeightPage', () => {
  let component: HeightPage;
  let fixture: ComponentFixture<HeightPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeightPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeightPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
