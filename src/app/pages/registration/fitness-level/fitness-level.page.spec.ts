import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessLevelPage } from './fitness-level.page';

describe('FitnessLevelPage', () => {
  let component: FitnessLevelPage;
  let fixture: ComponentFixture<FitnessLevelPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitnessLevelPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FitnessLevelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
