import { Component, OnInit } from '@angular/core';
import { GoalService } from 'src/app/services/goal/goal.service';
import { Goal } from 'src/app/models/goal.model';
import { Workout } from 'src/app/models/workout.model';
// import { ExerciseAction } from 'src/app/enums/exercise-action.enum';

@Component({
  selector: 'app-goal',
  templateUrl: './goal.page.html',
  styleUrls: ['./goal.page.css'],
})
export class GoalPage implements OnInit {
  get goal(): Goal | undefined {
    console.log('PROGRAM: ', this.goalService.lastGoal?.program);
    return this.goalService.lastGoal;
  }

  get workouts(): Workout[] {
    return this.goalService.workouts;
  }

  constructor(private readonly goalService: GoalService) {}

  ngOnInit(): void {
    this.goalService.fetchLastGoal();
  }
}
