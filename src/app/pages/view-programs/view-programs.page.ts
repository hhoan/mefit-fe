import { Component, Input, OnInit } from '@angular/core';
import { Program } from 'src/app/models/program.model';
import { ProgramService } from 'src/app/services/program/program.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-programs',
  templateUrl: './view-programs.page.html',
  styleUrls: ['./view-programs.page.css'],
})
export class ViewProgramsPage implements OnInit {
  get programs(): Program[] {
    return this.programService.programs;
  }

  constructor(private readonly programService: ProgramService, private router : Router) {
    /*this.router.navigateByUrl('/goal', { skipLocationChange: true }).then(() => {
      this.router.navigate(['programs']);
  })*/  ; 
  }

  ngOnInit(): void {
    this.programService.findAllPrograms();
  }
}
