import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProgramsPage } from './view-programs.page';

describe('ChooseProgramComponent', () => {
  let component: ViewProgramsPage;
  let fixture: ComponentFixture<ViewProgramsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewProgramsPage],
    }).compileComponents();

    fixture = TestBed.createComponent(ViewProgramsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
