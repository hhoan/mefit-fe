// modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpAuthInterceptor } from './interceptors/auth-http.interceptor';
import { RefreshTokenHttpInterceptor } from './interceptors/refresh-token-http.interceptor';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';

// components
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { GoalPage } from './pages/goal/goal.page';
import { ViewProgramsPage } from './pages/view-programs/view-programs.page';
import { ProgramListItemComponent } from './components/program-list-item/program-list-item.component';
import { AgePage } from './pages/registration/age/age.page';
import { HeightPage } from './pages/registration/height/height.page';
import { WeightPage } from './pages/registration/weight/weight.page';
import { FitnessLevelPage } from './pages/registration/fitness-level/fitness-level.page';
import { FitnessGoalPage } from './pages/registration/fitness-goal/fitness-goal.page';
import { RegistrationPage } from './pages/registration/registration/registration.page';
import { ExerciseItem } from './components/goal-exercise-item/goal-exercise-item.component';
import { ExerciseList } from './components/goal-exercise-list/goal-exercise-list.component';
import { FormsModule } from '@angular/forms';
import { ProgramListComponent } from './components/program-list/program-list.component';
import { SetProgramAsGoalButtonComponent } from './components/set-program-as-goal-button/set-program-as-goal-button.component';
import { PeriodComponent } from './components/period/period.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FaqPage } from './pages/faq/faq.page';
import { UserProfilePage } from './pages/UserProfile/user-profile/user-profile.page';
import { RegisterComponent } from './components/registration/register/register.component';
import { ProfileComponent } from './components/UserProfile/profile/profile.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { ProfileInfoComponent } from './components/goalDashboard/profile-info/profile-info.component';
import { WorkoutCheckboxComponent } from './components/workout-checkbox/workout-checkbox.component';
import { PopupSaveButtonComponent } from './components/popup-save-button/popup-save-button.component';
// import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    GoalPage,
    LoginPage,
    AgePage,
    HeightPage,
    WeightPage,
    FitnessLevelPage,
    FitnessGoalPage,
    RegistrationPage,
    ExerciseItem,
    ExerciseList,
    ViewProgramsPage,
    ProgramListItemComponent,
    ProgramListComponent,
    SetProgramAsGoalButtonComponent,
    PeriodComponent,
    NavbarComponent,
    FaqPage,
    UserProfilePage,
    RegisterComponent,
    ProfileComponent,
    ProgressBarComponent,
    ProfileInfoComponent,
    WorkoutCheckboxComponent,
    PopupSaveButtonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatSnackBarModule,
  ],
  providers: [
    // Check Token Before each request
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenHttpInterceptor,
      multi: true,
    },
    // Add HttpAuthInterceptor - Add Bearer Token to request
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
