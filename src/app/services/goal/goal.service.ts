import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { Goal } from 'src/app/models/goal.model';
import { Workout } from 'src/app/models/workout.model';
import { environment } from 'src/environments/environment';

const { apiGoals } = environment;

@Injectable({
  providedIn: 'root',
})
export class GoalService {
  private _lastGoal?: Goal;
  private _loading: boolean = false;
  private _error: string = '';
  private _workouts: Workout[] = [];

  get lastGoal(): Goal | undefined {
    return this._lastGoal;
  }

  
  get workouts(): Workout[] {
    return this._workouts;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) {}

  public createGoal(
    userId: string,
    endDate: Date,
    programId: number
  ): Observable<Goal> {
    const goal = {
      userProfile_id: userId,
      endDate: endDate,
      goalProgram_Id: programId,
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    // create goal in the db
    return this.http.post<Goal>(apiGoals, goal, { headers });
  }

  public fetchLastGoal(): void {
    // get last goal from API
    this._loading = true;
    this.http
      .get<Goal>(apiGoals + '/last')
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (goal: Goal) => {
          this._lastGoal = goal;
          this._workouts = goal.program.workouts;
          console.log('workout get', this._workouts);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }
}
