import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ExerciseModel} from 'src/app/models/exercises.model';
import { finalize, map } from 'rxjs';
const { apiExercises } = environment;

@Injectable({
  providedIn: 'root',
})
export class ExerciseService {
  //
  // private _oneExercise: ExerciseTryModel = ;
  private _exercises: ExerciseModel[] = []; //store exercises in array
  private _exercises2: ExerciseModel[] = [];
  private _exercises3: ExerciseModel[] = [];

  private _error: string = ''; //error initialize as empty string.
  private _loading: boolean = false;

  get exercises(): ExerciseModel[] {
    // can use readonly and this get-method to obtain exercises
    return this._exercises; // set exercises as property
  }
  get exercises2(): ExerciseModel[] {
    // can use readonly and this get-method to obtain exercises
    return this._exercises2; // set exercises as property
  }
  get exercises3(): ExerciseModel[] {
    // can use readonly and this get-method to obtain exercises
    return this._exercises3; // set exercises as property
  }

  get error(): string {
    // can use this to expose error msg.
    return this._error;
  }

  get loading(): boolean {
    // set loading as a property
    return this._loading;
  }

  constructor(private readonly http: HttpClient) {}

  public findExercisesForUser(workoutId : number): void {
    if (this._exercises.length > 0) return;

    this._loading = true;

    this.http
      .get<ExerciseModel[]>(`${apiExercises}` + `/${workoutId}/exercises`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: ExerciseModel[]) => {
          this._exercises = obj; //obj.res;
          console.log('exercises', obj);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }

  public findExercisesForUser2(workoutId : number): void {
    if (this._exercises2.length > 0) return;

    this._loading = true;

    this.http
      .get<ExerciseModel[]>(`${apiExercises}` + `/${workoutId}/exercises`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: ExerciseModel[]) => {
          this._exercises2 = obj; //obj.res;
          console.log('exercises', obj);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }

  public findExercisesForUser3(workoutId : number): void {
    if (this._exercises3.length > 0) return;

    this._loading = true;

    this.http
      .get<ExerciseModel[]>(`${apiExercises}` + `/${workoutId}/exercises`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: ExerciseModel[]) => {
          this._exercises3 = obj; //obj.res;
          console.log('exercises', obj);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }

  // For just one exercise
  public findOneExerciseForUser(): void {
    if (this._exercises.length > 0) return;

    this._loading = true;

    this.http
      .get<ExerciseModel>(`${apiExercises}/1`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: ExerciseModel) => {
          this._exercises.push(obj);
          console.log('exercise', this.exercises);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }
}
