import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Program } from '../../models/program.model';

const { apiPrograms } = environment;

@Injectable({
  providedIn: 'root',
})
export class ProgramService {
  private _program_list: Program[] = [];
  private _loading: boolean = false;
  private _error: string = '';

  get programs(): Program[] {
    return this._program_list;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) {}

  public findAllPrograms(): void {
    // get programs from API
    this._loading = true;
    this.http
      .get<Program[]>(apiPrograms)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (programs: Program[]) => {
          this._program_list = programs;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }
}
