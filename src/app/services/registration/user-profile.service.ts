import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of, switchMap, lastValueFrom, tap } from 'rxjs';
import { UserProfile } from 'src/app/models/UserProfile.model';
import { map, finalize, catchError, throwError, throwIfEmpty } from 'rxjs';
import keycloak from 'src/keycloak';

const { apiUserProfile } = environment;

@Injectable({
  providedIn: 'root',
})
export class UserProfileService {
  constructor(private readonly http: HttpClient) {}
  private _UserProfiles: UserProfile[] = []; //store exercises in array
  private _error: string = ''; //error initialize as empty string.
  private _loading: boolean = false;
  private _weight: number = 0;
  private _height: number = 0;
  private _fitnessGoal: string = "";
  private _fitnessLevel: string = "";

  

  get UserProfiles(): UserProfile[] {
    return this._UserProfiles;
  }

  
  get error(): string {
    // can use this to expose error msg.
    return this._error;
  }

  get loading(): boolean {
    // set loading as a property
    return this._loading;
  }

  public FindUserProfiles(): void {
    if (this._UserProfiles.length > 0) return;

    this._loading = true;

    this.http
      .get<UserProfile[]>(`${apiUserProfile}`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: UserProfile[]) => {
          this._UserProfiles = obj; //obj.res;
          console.log('UserProfiles ', obj);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }

  // Find one userprofile
  public FindOneUserProfile(Id: string): void {
    if (this._UserProfiles.length > 0) return;

    this._loading = true;

    this.http
      .get<UserProfile>(`${apiUserProfile}/${Id}`)
      .pipe(
        finalize(
          () => {
            this._loading = false;
          }
          // option map
        )
      )
      .subscribe({
        next: (obj: UserProfile) => {
          this._UserProfiles.push(obj);
          console.log('User Profile: ', this.UserProfiles);
        },
        error: (error: HttpErrorResponse) => {
          // when something goes wrong
          this._error = error.message;
        },
      });
  }

  public PostUserProfile(userProfile: UserProfile): Observable<UserProfile> {
      const headers = new HttpHeaders();
      headers.set('Authorization', 'Bearer ' + keycloak.token);
      headers.set('Content-Type', 'application/json');
      return this.http.post<UserProfile>(apiUserProfile, userProfile,)
        .pipe(
          catchError((error: HttpErrorResponse) => {
          console.log('HTTP request failed:', error);
          // Handle error
          return throwError(error.message || 'Server error');
        }),
        throwIfEmpty(() => new Error('Server error: Response is empty'))
      );
  }
  
  public async UpdateProfileInfo(data: any, id: string): Promise<UserProfile> {
    try {
      const updatedProfile$ = this.http.put<UserProfile>(`${apiUserProfile}/${id}`, data);
      const updatedProfile = await lastValueFrom(updatedProfile$);
      console.log('User profile updated:', updatedProfile);
      return updatedProfile;
    } catch (error) {
      console.error('Failed to update user profile:', error);
      throw error;
    }
  }
  
  private getUserProfile(id: string): Observable<UserProfile> {
    console.log(`getUserProfile(${id}) called`);
    return this.http.get<UserProfile>(`${apiUserProfile}/${id}`)
        .pipe(
            tap(response => console.log(`API response: ${JSON.stringify(response)}`)),
            catchError(error => {
                console.error(`API error: ${JSON.stringify(error)}`);
                return throwError(error);
            })
        );
  }

  public async getUserHeight(id: string): Promise<number> {
    const userProfile = await this.getUserProfile(id).toPromise();
    if (userProfile !== undefined)
    {
      this._height = userProfile.height;
      return this._height;
    }
    console.error(`User profile not found`);
    throw new Error('User profile not found');
  }

  public async getUserWeight(id: string): Promise<number> {
    console.log(`getUserWeight(${id}) called`);
    const userProfile = await this.getUserProfile(id).toPromise();
    console.log(`getUserProfile(${id}) returned: ${JSON.stringify(userProfile)}`);
    if (userProfile !== undefined) {
      this._weight = userProfile.weight;
      console.log(`Returning weight: ${this._weight}`);
      return this._weight;
    } 
    console.error(`User profile not found`);
    throw new Error('User profile not found');
  }

  public async getUserFitnessLevel(id: string): Promise<string> {
    const userProfile = await this.getUserProfile(id).toPromise();
    if (userProfile !== undefined) {
      this._fitnessLevel = userProfile.fitnessLevel;
      return this._fitnessLevel;
    }
    throw new Error('User profile not found');
  }

  public async getUserFitnessGoal(id: string): Promise<string> {
    const userProfile = await this.getUserProfile(id).toPromise();
    if (userProfile !== undefined) {
      this._fitnessGoal = userProfile.fitnessGoal;
      return this._fitnessGoal;
    }
    throw new Error('User profile not found');
  }

}