import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Workout } from 'src/app/models/workout.model';
import { environment } from 'src/environments/environment';

const { apiWorkouts } = environment;

@Injectable({
  providedIn: 'root',
})
export class WorkoutService {
  constructor(private readonly http: HttpClient) {}

  public UpdateWorkout(workout: Workout): void {
    this.http
      .put<Workout>(`${apiWorkouts}/${workout.workout_id}`, workout)
      .subscribe((workout) => {
        console.log('PUT call successful ', workout);
      });
  }
}
