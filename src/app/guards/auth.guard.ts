import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  //UrlTree,
} from '@angular/router';
//import { Observable } from 'rxjs';
import keycloak from 'src/keycloak';

export const authGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const router = inject(Router);
  // Authenticated! Yay - Continue.
  if (keycloak.authenticated) {
    return true;
  }
  // Not authenticated - Automatically navigate back to Login
  router.navigateByUrl('/login');
  return false;
};
