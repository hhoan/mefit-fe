import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { UserProfileService } from 'src/app/services/registration/user-profile.service';
import keycloak from 'src/keycloak';
import { UserProfile } from 'src/app/models/UserProfile.model';
import { Router } from '@angular/router';

interface FitnessLevel {
  value: string;
  viewValue: string;
}

interface FitnessGoal {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  constructor(
    private readonly userProfileService: UserProfileService,
    private router: Router
  ) {}

  selectedFitnessLevelValue: string = '';
  selectedFitnessGoalValue: string = '';

  FitnessLevels: FitnessLevel[] = [
    { value: 'Untrained', viewValue: 'Untrained' },
    { value: 'Balanced', viewValue: 'Balanced' },
    { value: 'FitnessKing', viewValue: 'Fitness King' },
  ];

  FitnessGoals: FitnessGoal[] = [
    { value: 'Lose Fat', viewValue: 'Lose Fat' },
    { value: 'Healthy Lifestyle', viewValue: 'Healthy Lifestyle' },
    { value: 'IncreaseMuscle', viewValue: 'Increase Muscle Mass' },
  ];

  async submit(register: NgForm) {
    try {
      let KeycloakId = keycloak.subject || '';
      let { Weight } = register.value;
      let { Height } = register.value;

      console.log(KeycloakId);
      console.log(Weight);
      console.log(Height);

      const newProfile: UserProfile = {
        id: KeycloakId,
        weight: register.value.Weight,
        height: register.value.Height,
        fitnessLevel: this.selectedFitnessLevelValue,
        fitnessGoal: this.selectedFitnessGoalValue
      };
       //---------------------------------- POST TO Database
       this.userProfileService.PostUserProfile(newProfile).subscribe({
        next: (result) => {
          console.log('Successfully created user profile:', result);
          // Do something with the result, if needed
        },
        error: (err) => {
          console.error('Failed to create user profile:', err);
          // Handle the error appropriately
        },
        complete: () => {
          console.log('User profile creation request completed.');
          // Do something when the request is completed, if needed
        },
      });
      //navigate to choose-program after registration
      this.router.navigateByUrl('/programs');
    } catch (error) {
      console.error(error);
      // Handle the error appropriately
    }
  }
}
