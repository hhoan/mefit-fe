import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutCheckboxComponent } from './workout-checkbox.component';

describe('WorkoutCheckboxComponent', () => {
  let component: WorkoutCheckboxComponent;
  let fixture: ComponentFixture<WorkoutCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutCheckboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkoutCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
