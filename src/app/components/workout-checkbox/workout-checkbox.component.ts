import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Workout } from 'src/app/models/workout.model';
import { WorkoutService } from 'src/app/services/workout/workout.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-workout-checkbox',
  templateUrl: './workout-checkbox.component.html',
  styleUrls: ['./workout-checkbox.component.css'],
})
export class WorkoutCheckboxComponent implements OnInit {
  @Input() workout?: Workout;
  @Output() notifyProgress: EventEmitter<number> = new EventEmitter<number>();

  value = 0;
  checked?: boolean = false;

  checkbox = { label: 'Checkbox 1', value: 33.3 };

  constructor(private readonly workoutService: WorkoutService) {}

  ngOnInit(): void {
    this.checked = this.workout?.complete;
    if (this.checked) {
      this.updateValue();
    }
  }

  updateCompleted() {
    if (this.workout) {
      this.workout.complete = !this.workout.complete;
      console.log('NEW WORKOUT: ', this.workout);
      this.workoutService.UpdateWorkout(this.workout);
      this.updateValue();
      this.checked = this.workout.complete;
      this.notifyProgress.emit(34);
    }
  }

  updateValue() {
    this.value = this.checkbox.value;
  }

  updateCheckbox(){
      this.checked = true;
      this.notifyProgress.emit(34);
  }


}
