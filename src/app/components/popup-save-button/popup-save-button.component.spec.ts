import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupSaveButtonComponent } from './popup-save-button.component';

describe('PopupSaveButtonComponent', () => {
  let component: PopupSaveButtonComponent;
  let fixture: ComponentFixture<PopupSaveButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupSaveButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopupSaveButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
