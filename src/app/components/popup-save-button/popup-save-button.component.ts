// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-popup-save-button',
//   templateUrl: './popup-save-button.component.html',
//   styleUrls: ['./popup-save-button.component.css']
// })
// export class PopupSaveButtonComponent {

// }



import {Component} from '@angular/core';
import {  MatSnackBar,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
/**
 * @title Snack-bar with configurable position
 */
@Component({
  selector: 'app-popup-save-button',
  templateUrl: './popup-save-button.component.html',
  styleUrls: ['./popup-save-button.component.css'],
})
export class PopupSaveButtonComponent {
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private _snackBar: MatSnackBar) {}

  // openSnackBar() {
  //   this._snackBar.open('Cannonball!!', 'Splash', { horizontalPosition: this.horizontalPosition, verticalPosition: this.verticalPosition,
  //   });
  // }

  openSnackBar() {
    this._snackBar.open('Cannonball!!', 'Splash', { duration : 2000});
  }


}
