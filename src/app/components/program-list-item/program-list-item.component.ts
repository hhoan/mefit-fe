import { Component, Input } from '@angular/core';
import { Program } from 'src/app/models/program.model';

@Component({
  selector: 'app-program-list-item',
  templateUrl: './program-list-item.component.html',
  styleUrls: ['./program-list-item.component.css'],
})
export class ProgramListItemComponent {
  @Input() program?: Program;
}
