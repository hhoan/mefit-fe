import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-period',
  templateUrl: './period.component.html',
  styleUrls: ['./period.component.css'],
})
export class PeriodComponent {
  @Input() endDateString?: string;

  private get endDate(): Date | undefined {
    if (this.endDateString) {
      return new Date(this.endDateString);
    } else {
      return undefined;
    }
  }

  get endDateDay(): number | undefined {
    if (this.endDate) {
      return this.endDate.getDate();
    } else {
      return undefined;
    }
  }

  get endDateMonth(): number | undefined {
    if (this.endDate) {
      return this.endDate.getMonth() + 1;
    } else {
      return undefined;
    }
  }

  get endDateYear(): number | undefined {
    if (this.endDate) {
      return this.endDate.getFullYear();
    } else {
      return undefined;
    }
  }

  get startDate(): Date | undefined {
    if (this.endDate) {
      return new Date(this.endDate.getTime() - 7 * 24 * 60 * 60 * 1000);
    } else {
      return undefined;
    }
  }

  get startDateDay(): number | undefined {
    if (this.startDate) {
      return this.startDate.getDate();
    } else {
      return undefined;
    }
  }

  get startDateMonth(): number | undefined {
    if (this.startDate) {
      return this.startDate.getMonth() + 1;
    } else {
      return undefined;
    }
  }

  get startDateYear(): number | undefined {
    if (this.startDate) {
      return this.startDate.getFullYear();
    } else {
      return undefined;
    }
  }
}
