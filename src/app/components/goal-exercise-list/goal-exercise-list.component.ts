import { Component, Input, OnInit } from '@angular/core';
import { ExerciseModel } from 'src/app/models/exercises.model';
import { Goal } from 'src/app/models/goal.model';
import { Workout } from 'src/app/models/workout.model';
import { ExerciseService } from 'src/app/services/exercise/goal-exercise.service';
import { ExerciseItem } from '../goal-exercise-item/goal-exercise-item.component';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
@Component({
  selector: 'app-exercise-list',
  templateUrl: 'goal-exercise-list.component.html',
  styleUrls: ['goal-exercise-list.component.css'],
})
export class ExerciseList implements OnInit {
  @Input() userWorkouts: Array<any> = []; // Workout[] | undefined;


// this is for progressbar
  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'determinate';
  value? : number;
  value1 : number = 0;
  value2 : number = 0;
  value3 : number = 0;

  


  items: ExerciseModel[] = [];
  private descriptionBool: boolean = false;
  private _description: string | undefined = '';
  private _updateWorkoutBool: boolean = false;

  constructor(private readonly exerciseService: ExerciseService) {}

  get exercises(): ExerciseModel[] {
    return this.exerciseService.exercises;
  }

  get exercises2(): ExerciseModel[] {
    return this.exerciseService.exercises2;
  }

  get exercises3(): ExerciseModel[] {
    return this.exerciseService.exercises3;
  }

  get updateWorkoutBool(): boolean {
    return this._updateWorkoutBool;
  }

  get userWorkoutsList() {
    return this.userWorkouts;
  }

  get workout1(): Workout {
    return this.userWorkouts[0];
  }

  get workout2(): Workout {
    return this.userWorkouts[1];
  }

  get workout3(): Workout {
    return this.userWorkouts[2];
  }


//this is for checkbox 
  getValueFromCheckbox1(evt : number)
  { 
    if (this.value1 == 34){
    this.value1= 0;
    this.valueSum();
     } else 
      {
      this.value1= 34;
      this.valueSum();
    }
    
  }

  getValueFromCheckbox2(evt : number)
  {
    if (this.value2 == 34){
      this.value2= 0;
      this.valueSum();
       } else 
        {
        this.value2= 34;
        this.valueSum();
      }
  }

  getValueFromCheckbox3(evt : number)
  {
    if (this.value3 == 34){
      this.value3= 0;
      this.valueSum();
       } else 
        {
        this.value3= 34;
        this.valueSum();
      }
  }


  valueSum(){
    return this.value = this.value1 + this.value2 + this.value3;
  }


  getShowDescription(evt: string | undefined) {
    console.log('Show description');
    console.log('this event: ', evt);

    if (this.descriptionBool == true) {
      this.descriptionBool = false;
    } else if (typeof evt != undefined) {
      this.descriptionBool = true;
      this._description = evt;
    }
    console.log(this.descriptionBool);
  }

  get showDescription() {
    return this.descriptionBool;
  }

  get description() {
    return this._description;
  }

  get loading(): boolean {
    // expose loading: true/false
    return this.exerciseService.loading;
  }

  get error(): string {
    return this.exerciseService.error;
  }


  ProgressBarOnStart()
  {
    if (this.workout1.complete)
    {
      this.value1 = 34;
    }
    if (this.workout2.complete)
    {
      this.value2 = 34;
    }
    if (this.workout3.complete)
    {
      this.value3 = 34;
    }
    this.valueSum();
  }


  ngOnInit(): void {
    console.log('workout in ex list', this.userWorkouts);
    // todo: allow for more than three workouts and add workouts to a program
    let id1 = this.userWorkoutsList[0].workout_id; //["0"]["workout_id"];
    let id2 = this.userWorkoutsList[1].workout_id; // ["1"]["workout_id"];
    let id3 = this.userWorkouts[2].workout_id; // ["2"]["workout_id"];
    let list = [id1, id2, id3];

    this._updateWorkoutBool = false;
    this.exerciseService.findExercisesForUser(list[0]);
    this.exerciseService.findExercisesForUser2(list[1]);
    this.exerciseService.findExercisesForUser3(list[2]);
    this._updateWorkoutBool = true;
    this.ProgressBarOnStart();
  }
}
