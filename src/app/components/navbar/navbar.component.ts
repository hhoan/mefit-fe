import { Component } from '@angular/core';
import { Router } from '@angular/router';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(
    private readonly router: Router
  ){}

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  handleLogoutClick(): void {
    if (this.authenticated) {
      if (window.confirm('Are you sure?')) {
        keycloak.logout();
        this.router.navigateByUrl('/login');
      }
  }

  }
}
