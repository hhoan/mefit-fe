import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { UserProfileService } from 'src/app/services/registration/user-profile.service';
import keycloak from 'src/keycloak';
import { UserProfile } from 'src/app/models/UserProfile.model';
import {  MatSnackBar,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition } from '@angular/material/snack-bar';




interface FitnessLevel {
  value: string;
  viewValue: string;
}

interface FitnessGoal {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  constructor(private readonly userProfileService: UserProfileService, private _snackBar: MatSnackBar){ }
  Id: string = keycloak.subject || "";
  Weight?: number; 
  Height?: number;
  FitnessLevel?: string;
  FitnessGoal?: string;
  FirstName?: string;
  LastName?: string;
  Email?: string;


  selectedFitnessLevelValue?: string = this.FitnessLevel;
  selectedFitnessGoalValue?: string = this.FitnessGoal;

  FitnessLevels: FitnessLevel[] = [
    { value: 'Untrained', viewValue: 'Untrained' },
    { value: 'Balanced', viewValue: 'Balanced' },
    { value: 'FitnessKing', viewValue: 'Fitness King' },
  ];

  FitnessGoals: FitnessGoal[] = [
    { value: 'Lose Fat', viewValue: 'Lose Fat' },
    { value: 'Healthy Lifestyle', viewValue: 'Healthy Lifestyle' },
    { value: 'IncreaseMuscle', viewValue: 'Increase Muscle Mass' },
  ];

 
    ngOnInit(): void {
      this.userProfileService.getUserWeight(this.Id).then((Weight: number) => {
        console.log('Weight:', Weight);
        this.Weight = Weight || 0;
      }).catch((error: Error) => {
        console.error(error);
      });

      this.userProfileService.getUserHeight(this.Id).then((Height:number) => {
        console.log('Height:', Height);
        this.Height = Height || 0;
      }).catch((error: Error) => {
        console.error(error);
      });

      this.userProfileService.getUserFitnessLevel(this.Id).then((FitnessLevel:string) => {
        console.log('FitnessLevel:', FitnessLevel);
        this.FitnessLevel = FitnessLevel || "";
        this.selectedFitnessLevelValue = this.FitnessLevel;
      }).catch((error: Error) => {
        console.error(error);
      });

      this.userProfileService.getUserFitnessGoal(this.Id).then((FitnessGoal:string) => {
        console.log('FitnesGoal:', FitnessGoal);
        this.FitnessGoal = FitnessGoal || "";
        this.selectedFitnessGoalValue = this.FitnessGoal;
      }).catch((error: Error) => {
        console.error(error);
      });

      this.FirstName = keycloak.tokenParsed?.given_name;
      this.LastName = keycloak.tokenParsed?.family_name;
      this.Email = keycloak.tokenParsed?.email;
    }

    async update(updateProfile: NgForm)
    {
        const UpdatedProfile: UserProfile = {
        id: keycloak.subject || "",
        weight: Number(updateProfile.value.Weight),
        height: Number(updateProfile.value.Height),
        fitnessLevel: this.selectedFitnessLevelValue || "",
        fitnessGoal: this.selectedFitnessGoalValue || ""
        };
        //------------------------PATCH/PUT legg denne i userProfile component 
        const result = await this.userProfileService.UpdateProfileInfo(UpdatedProfile, this.Id);
        
    }
    openSnackBar()
    {
      this._snackBar.open('Saved!!', 'saved', { duration : 2000});
    }
    
    
}
