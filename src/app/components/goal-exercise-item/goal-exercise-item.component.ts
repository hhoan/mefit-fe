import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ExerciseModel} from 'src/app/models/exercises.model';
import { ExerciseService } from 'src/app/services/exercise/goal-exercise.service';

@Component({
  selector: 'app-exercise-item',
  templateUrl: 'goal-exercise-item.component.html',
  styleUrls: ['goal-exercise-item.component.css'],
})
export class ExerciseItem implements OnInit {
  @Input() exercise?: ExerciseModel;
  private descriptionBool : boolean = false;
  private _description : string | undefined =  "";

  @Output() notifyParent: EventEmitter<string> = new EventEmitter<string>();

  constructor(private readonly exerciseService: ExerciseService) {}

  btnShowDescription() {
      this.notifyParent.emit(this.exercise?.description);
      
      /*if (this.descriptionBool == true){
        this.descriptionBool = false;
      }*/
      //else 
      if (typeof(this.exercise?.description) != undefined) {
        this.descriptionBool = true;
        this._description = this.exercise?.description;
      }
      // console.log(this.descriptionBool)
    }
  
  get description() {
    return this._description;
  }

  ngOnInit() {
    // Called after the constructor and called  after the first ngOnChanges()
  }
}
