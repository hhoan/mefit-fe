import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Program } from 'src/app/models/program.model';
import { GoalService } from 'src/app/services/goal/goal.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-set-program-as-goal-button',
  templateUrl: './set-program-as-goal-button.component.html',
  styleUrls: ['./set-program-as-goal-button.component.css'],
})
export class SetProgramAsGoalButtonComponent {
  @Input() program?: Program;

  constructor(
    private readonly goalService: GoalService,
    private router: Router
  ) {}

  onStartProgramClick() {
    if (this.program != null) {
      // set up arguments for createGoal
      const userId = keycloak.subject; // assume not undefined, because of auth guard
      const programId = this.program.program_id;
      const date = new Date(); // today
      date.setDate(date.getDate() + 7);
      const endDate = date; // 7 days from today

      this.goalService
        .createGoal(String(userId), endDate, programId)
        .subscribe({
          error: (error: HttpErrorResponse) => {
            console.error('Error: ', error.message);
          },
        });

      this.router.navigateByUrl('/goal');
    }
  }
}
