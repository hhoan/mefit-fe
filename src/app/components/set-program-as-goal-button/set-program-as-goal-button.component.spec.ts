import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetProgramAsGoalButtonComponent } from './set-program-as-goal-button.component';

describe('SetProgramAsGoalButtonComponent', () => {
  let component: SetProgramAsGoalButtonComponent;
  let fixture: ComponentFixture<SetProgramAsGoalButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetProgramAsGoalButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetProgramAsGoalButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
