import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './guards/auth.guard';
//import { RoleGuard } from './guards/role.guard';
import { LoginPage } from './pages/login/login.page';
import { GoalPage } from './pages/goal/goal.page';
import { RegistrationPage } from './pages/registration/registration/registration.page';
import { UserProfilePage } from './pages/UserProfile/user-profile/user-profile.page';
import { ViewProgramsPage } from './pages/view-programs/view-programs.page';
import { FaqPage } from './pages/faq/faq.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/goal'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'goal', 
    component: GoalPage,
    canActivate: [authGuard]
  },
  {
    path: 'registration',
    component: RegistrationPage,
    canActivate: [authGuard],
  },
  {
    path: "userprofile",
    component: UserProfilePage,
    canActivate: [authGuard],
  },  
  {
    path: 'programs',
    component: ViewProgramsPage,
    canActivate: [authGuard],
  },
  {
    path: 'faq',
    component: FaqPage,
  },
  // {
  //   path: 'admin/dashboard',
  //   component: AdminDashboardPage,
  //   canActivate: [RoleGuard],// Protected by Keycloak Role: ADMIN
  //   data: {
  //     role: "ADMIN"
  //   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
